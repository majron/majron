let Embed = require("../utils/Embed");


module.exports = {
  name: "config",
  execute: async (client, message, args, env) => {
    let allowedConfigs = [
      {
        name: "prefix",
        example: "!config prefix ?",
        default: "!"
      }
    ];
    let embedFields = [];
    let guildQuery = client.magoClient.query({id: message.guild.id});
    async function sendList() {
      let messageEmbed = new Embed();
      let query = await guildQuery.find();
      allowedConfigs.forEach((config) => {
        embedFields.push({
          name: config.name,
          value: `Value: "${query.prefix}" | Example: ${config.example}`
        });
      });
      messageEmbed.addFields(embedFields);
      message.channel.send(messageEmbed);
    };

    if (!args[0]) { 
      sendList();
    } else if (args[0] && !allowedConfigs.find(cfg => cfg.name == args[0])) {
      sendList();
    } else {
      let messageEmbed = new Embed();
      await guildQuery.update({$set: {[args[0]]: args[1] || allowedConfigs.find(cfg => cfg.name == args[0]).default}});
      messageEmbed.setDescription("Done!");
      message.channel.send(messageEmbed);
    };
  },
  desc: "Used for bot config for the server",
  groups: [
    "DEVELOPER",
    "OWNER"
  ],
  catagory: "server",
  types: [
    "text"
  ],
  alias: [
    "cfg"
  ]
};