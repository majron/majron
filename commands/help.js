let Embed = require("../utils/Embed");
module.exports = {
  name: "help",
  execute: (client, message, args, env) => {
    let Name = args[0] || ""
    let Commands = env.Commands;
    if (Commands.find(Command => Command.catagory == Name)) {
      let EmbedCommands = [];
      Commands.forEach((Command) => {
        if (Command.catagory == Name) {
          EmbedCommands.push({
            name: `\`${env.prefix}help ${Command.name}\``,
            value: `${Command.name} command`,
            inline: true
          });
        };
      });

      let embedMessage = new Embed();
      embedMessage.addFields(EmbedCommands);
      message.channel.send(embedMessage);
    } else if (Commands.find(Command => Command.name == Name)) {
      let Command = Commands.find(Command => Command.name == Name);
      let embedMessage = new Embed();
      embedMessage.setDescription(`Command for "${Name}"`);
      embedMessage.addField("Description", Command.desc);
      message.channel.send(embedMessage);
    } else {
      let embedMessage = new Embed();
      let embedCatagorys = [];
      let insertedCatagorys = [];
      Commands.forEach((Command) => {
        if (!insertedCatagorys.includes(Command.catagory)) {
          insertedCatagorys.push(Command.catagory);
          embedCatagorys.push({
            name: `\`${env.prefix}help ${Command.catagory}\``,
            value: `${Command.catagory} commands`,
            inline: true
          });
        };
      });
      embedMessage.addFields(embedCatagorys);
      embedMessage.setDescription("Catagorys available for you to check");
      message.channel.send(embedMessage);
    };
  },
  desc: "Lists avaliable commands that you can use",
  catagory: "info",
  types: [
    "text"
  ],
  alias: [
    "cmds"
  ]
};