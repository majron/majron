let Embed = require("../utils/Embed");
module.exports = {
  name: "purge",
  execute: async (client, message, args, env) => {
    if (!args[0]) return message.channel.send("Amount argument is required");
    let Amount = (Number(args[0]) + 1);
    if (Amount >= 100) {
      for (var i = 0; i < Math.floor(Amount / 100); i++) {
        await message.channel.bulkDelete(100);
      };
      let OtherMessagesToDelete = (Amount / 100);
      if (OtherMessagesToDelete > Math.floor(Amount / 100)) {
        let NumOther = parseInt(OtherMessagesToDelete.toString().split(".")[1]);
        await message.channel.bulkDelete(NumOther * (NumOther > 10 && 1 || 10));
      }
    } else {
      await message.channel.bulkDelete(Amount);
    };
    let embedMessage = new Embed();
    embedMessage.setDescription("Done purging!");
    message.channel.send(embedMessage);
  },
  desc: "Purges messages",
  groups: [
    "MODERATOR",
    "DEVELOPER"
  ],
  catagory: "moderation",
  types: [
    "text"
  ],
  alias: [
    "delete"
  ]
};