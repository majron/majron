const Embed = require("../utils/Embed");

module.exports = {
  name: "snipe",
  execute: (client, message, args, env) => {
    let embedMessage = new Embed();
    embedMessage.setDescription(`Deleted message: "${client.recentDeletedMsgs[message.channel.id] || "Not Found"}"`);
    message.channel.send(embedMessage);
  },
  desc: "Sends the recent deleted message",
  groups: [
    "DEVELOPER",
    "MODERATOR"
  ],
  catagory: "moderation",
  types: [
    "text"
  ],
  alias: [
    "deletedmsg"
  ]
};