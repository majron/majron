const fs = require("fs");
const CommandHandler = require("../utils/CommandHandler");
const Handler = new CommandHandler();

fs.readdirSync(__dirname +  "/../commands").forEach((file) => {
  Handler.insertCommand(require(`../commands/${file.split(".")[0]}`));
});

module.exports = {
  name: "message",
  execute: async (client, message) => {
    let Args = message.content.split(" ");
    let CommandArgs = Args.slice(1);
    let config = {
      prefix: "!"
    }
    if (message.guild) {
      let query = client.magoClient.query({id: message.guild.id});
      var queryConfig = await query.find() || config

      if (!queryConfig._id) {
        config.id = message.guild.id;
        await client.magoClient.query(config).insert();
      } else {
        config = queryConfig;
      };
    };

    let CommandPrefix = Args[0].substr(0, config.prefix.length);
    let Name = Args[0].substr(config.prefix.length, Args[0].length);
    let Command = Handler.getCommand(Name);
    let Commands = Handler.getCommands();
    function insertGroup(group) {
      Command.insertUserGroup(group);
      Commands.insertUserGroup(group);
    };

    if (message.author.id == process.env.BOT_DEVELOPER) {
      insertGroup("DEVELOPER");
    };

    let Member = message.guild && message.guild.member(message.author) || false;

    if (Member && Member.hasPermission("ADMINISTRATOR")) {
      insertGroup("MODERATOR");
    };

    if (message.guild && message.guild.ownerID == message.author.id) {
      insertGroup("OWNER");
    };

    if (Command.fetch(message.channel.type) && CommandPrefix == config.prefix) {
      Command.fetch(message.channel.type).execute(client, message, CommandArgs, {
        Handler: Handler,
        Commands: Commands.fetch(message.channel.type),
        prefix: config.prefix
      });
    };
  }
};