require("dotenv").config();
const { Client } = require("discord.js");
let client = new Client();
const mago = require("./utils/mago");
const magoClient = new mago(process.env.MAGO_URL, process.env.MAGO_DB, process.env.MAGO_COLLECTION);
const fs = require("fs");
magoClient.connect();
client.magoClient = magoClient;
fs.readdirSync("./events").forEach((file) => { 
  let Event = require(`./events/${file.split(".")[0]}`);
  client.on(Event.name, (...args) => {
    Event.execute(client, ...args);
  });
});



client.recentDeletedMsgs = {};
client.login(process.env.TOKEN);