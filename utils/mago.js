const { MongoClient } = require("mongodb");
class mago {
  constructor(url, db, collection) {
    this.url = url;
    this.collection = collection;
    this.db = db;
    async function closeDb() {
      console.log("exiting");
      if (this.connection) {
        await this.connection.close();
      };
      process.exit();
    }
    process.on("SIGINT", closeDb);
    process.on("SIGQUIT", closeDb);
    process.on('SIGTERM', closeDb);
  }
  connect() {
    let classThis = this;
    return new Promise(async (resolve, reject) => {
      let Client = new MongoClient(this.url, {
        useUnifiedTopology: true
      });
      await Client.connect();
      classThis.connection = Client;
      resolve();
    })
  }
  query(query) {
    return {
      find: async (list) => {
        let DB = this.connection.db(this.db);
        let Collection = DB.collection(this.collection);
        if (list) {
          let Query = await Collection.find(query);
          return Query.toArray();
        } else {
          let Query = await Collection.findOne(query);
          return Query
        };
      },
      update: async (newQuery) => {
        let DB = this.connection.db(this.db);
        let Collection = DB.collection(this.collection);
        await Collection.updateOne(query, newQuery);
      },
      insert: async () => {
        let DB = this.connection.db(this.db);
        let Collection = DB.collection(this.collection);
        await Collection.insertOne(query);
      },
      delete: async () => {
        let DB = this.connection.db(this.db);
        let Collection = DB.collection(this.collection);
        await Collection.deleteOne(query);
      }
    }
  };
}

module.exports = mago;